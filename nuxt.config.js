import path from 'path'
import CompressionPlugin from 'compression-webpack-plugin'
import SpritesmithPlugin from 'webpack-spritesmith'
require('dotenv').config()
export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    bodyAttrs: {
      class: 'bg-body'
    },
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: ['~/assets/css/index.css'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [{ src: '~/plugins/VueFlickity.js', ssr: false }],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module',
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss'
    // '@teamnovu/nuxt-breaky'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    '@nuxtjs/device',
    '@nuxtjs/svg-sprite',
    [
      'nuxt-lazy-load',
      {
        // These are the default values
        images: true,
        directiveOnly: true,

        // Default image must be in the static folder
        defaultImage: '/default-image.jpg',

        // To remove class set value to false
        loadingClass: 'isLoading',
        loadedClass: 'isLoaded',
        appendClass: 'lazyLoad',

        observerConfig: {
          // See IntersectionObserver documentation
        }
      }
    ],
    ['portal-vue/nuxt']
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: process.env.BASE_URL
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extractCSS: true,
    extend(config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        const templateFunction = function(data) {
          const shared = '.images { display:inline-block; background-image: url(I); background-size:WSMpx HSMpx; }'
            .replace('I', data.sprites[0].image)
            .replace('WSM', data.spritesheet.width)
            .replace('HSM', data.spritesheet.height)

          const perSprite = data.sprites
            .map(function(sprite) {
              return '.image-N { width: Wpx; height: Hpx; background-position: Xpx Ypx; }'
                .replace('N', sprite.name)
                .replace('W', sprite.width)
                .replace('H', sprite.height)
                .replace('X', sprite.offset_x)
                .replace('Y', sprite.offset_y)
            })
            .join('\n')

          return shared + '\n' + perSprite
        }
        config.resolve.modules.push('./assets/images/sprites/')
        config.plugins.push(
          new SpritesmithPlugin({
            src: {
              cwd: path.resolve(__dirname, './assets/images/sprites/'),
              glob: '**/*.png'
            },
            target: {
              image: path.resolve(__dirname, './assets/images/sprites.png'),
              css: [
                [
                  path.resolve(__dirname, './assets/css/sprites.css'),
                  {
                    format: 'function_based_template'
                  }
                ]
              ]
            },
            customTemplates: {
              function_based_template: templateFunction
            },
            apiOptions: {
              cssImageRef: "'~assets/images/sprites.png'"
            },
            spritesmithOptions: {
              padding: 10
            }
          })
        )
      }
    },
    postcss: {
      preset: {
        autoprefixer: {
          flexbox: true
        }
      }
    },
    loaders: {
      cssModules: {
        modules: {
          mode: 'local',
          localIdentName: '[hash:base64:4]',
          hashPrefix: 'my-custom-hash'
        }
      }
    },
    plugins: [new CompressionPlugin()]
  },
  svgSprite: {
    input: '~/assets/icons/',
    spriteClassPrefix: 'icon-'
  },
  tailwindcss: {
    exposeConfig: true
  }
}
