/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
const defaultTheme = require('tailwindcss/defaultTheme')
module.exports = {
  theme: {
    fontFamily: {
      sans: ['Quarion', ...defaultTheme.fontFamily.sans]
    },
    container: {
      center: true,
      padding: {
        default: '10px'
      }
    },
    extend: {
      colors: {
        primary: '#053FFF',
        info: '#042261',
        dark: {
          default: '#03182D',
          cyan: '#0CA495',
          blue: '#000529',
          gray: '#0E162C'
        },
        light: {
          cyan: '#53F5E6',
          gray: '#EFF0F4'
        },
        pearl: '#03182d',
        blue: {
          default: '#053FFF',
          secondary: '#0069E4'
        },
        red: '#F73391',
        gray: {
          default: '#4B5161',
          dark: '#080D1B',
          medium: '#878B96',
          blue: '#DEE1E9',
          '100': '#C3C5CB',
          '200': '#EFF0F4',
          '300': '#F7F9FB'
        },
        green: {
          neon: '#2EFF75',
          spring: '#00E61E'
        },
        yellow: '#FFE822',
        orange: {
          light: '#FFAA29'
        },
        pink: {
          soft: '#D67DFF'
        },
        warning: '#FFC043'
      },
      backgroundColor: {
        '100': '#C3C5CB',
        body: '#F7F9FB',
        blue: '#053FFF',
        gray: {
          light: '#EFF0F4',
          '300': '#F7F9FB'
        },
        red: '#EC2A00',
        primary: {
          cyan: '#27CEBE'
        },
        dark: {
          cyan: '#0CA495',
          blue: '#000529'
        }
      },
      backgroundPosition: {
        'center-bottom': 'center bottom'
      },
      borderOpacity: {
        10: '0.10',
        15: '0.15'
      },
      borderColor: {
        blue: {
          default: '#042261',
          gray: '#DEE1E9'
        },
        gray: {
          default: '#4B5161',
          dark: '#080D1B',
          light: '#EFF0F4'
        }
      },
      borderRadius: {
        xl: '1rem'
      },
      lineHeight: {
        120: '1.2',
        140: '1.4',
        150: '1.5',
        160: '1.6',
        225: '2.25'
      },
      textOpacity: {
        70: '0.7',
        80: '0.8'
      },
      textShadow: {
        1: '-1px -1px 0 #53F5E6',
        2: '-1px -1px 0 #FFE500',
        3: '3px 0 0 #53F5E6',
        4: '-2px 0 0 #FF2FD1'
      },
      boxShadow: {
        1: '0px 2px 8px rgba(0, 0, 0, 0.05)',
        2: '0px 2px 12px rgba(0, 0, 0, 0.12)'
      },
      width: {
        '36px': '36px',
        '86px': '86px',
        '100px': '100px',
        '106px': '106px',
        '215px': '215px',
        '267px': '267px',
        '270px': '270px',
        '318px': '318px',
        '384px': '384px',
        '680px': '680px'
      },
      height: {
        '36px': '36px',
        '404px': '404px'
      },
      maxWidth: {
        '2xl': '42.5rem'
      },
      padding: {
        '7d5px': '7.5px',
        '10px': '10px',
        '30px': '30px',
        '13': '3.25rem'
      },
      margin: {
        '13': '3.25rem',
        '6px': '6px',
        '10px': '10px',
        '22px': '22px',
        '30px': '30px',
        '-7d5px': '-7.5px',
        '-10px': '-10px',
        '-30px': '-30px',
        '400px': '400px'
      },
      fontSize: {
        '9px': '0.563rem',
        '14px': '0.875rem',
        '15px': '0.938rem',
        '26px': '1.625rem',
        '28px': '1.75rem',
        '2rem': '2rem',
        '38px': '2.375rem',
        '7xl': '4.5rem'
      },
      animations: {
        fadeIn: {
          from: {
            opacity: '0'
          },
          to: {
            opacity: '1'
          }
        },
        fadeInUp: {
          '0%': {
            opacity: '0',
            transform: 'translateY(10px)'
          },
          '100%': {
            opacity: '1',
            transform: 'translateY(0%)'
          }
        }
      },
      animationDuration: {
        '200ms': '200ms',
        '0s': '0s',
        '1s': '1s',
        '2s': '2s',
        '3s': '3s',
        '4s': '4s',
        '5s': '5s'
      }
    }
  },
  variants: {
    padding: ['responsive', 'last', 'first'],
    margin: ['responsive', 'last', 'first'],
    borderWidth: ['responsive', 'last', 'first', 'hover', 'focus'],
    textShadow: ['responsive'],
    animations: ['responsive'],
    animationDuration: ['responsive'],
    animationTimingFunction: ['responsive'],
    animationDelay: ['responsive'],
    animationIterationCount: ['responsive'],
    animationDirection: ['responsive'],
    animationFillMode: ['responsive'],
    animationPlayState: ['responsive']
  },
  plugins: [
    require('tailwindcss-typography')({
      textShadow: true
    }),
    require('tailwindcss-animations')
  ],
  purge: {
    // Learn more on https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
    enabled: process.env.NODE_ENV === 'production',
    content: [
      'components/**/*.vue',
      'layouts/**/*.vue',
      'pages/**/*.vue',
      'plugins/**/*.js',
      'nuxt.config.js'
    ]
    // These options are passed through directly to PurgeCSS
    // options: {
    //   whitelist: ['bg-red-500', 'px-4']
    // }
  }
}
