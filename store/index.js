export const state = () => ({
  isMobile: false,
  MainMenu: []
})
export const mutations = {
  SET_IS_MOBILE: (state, payload) => (state.isMobile = payload),
  SET_MAINMENU: (state, payload) => (state.MainMenu = payload.reverse())
}
export const getters = {
  isMobile: (state) => state.isMobile,
  MainMenu: (state) => state.MainMenu
}
export const actions = {
  async getMainMenu({ commit }) {
    const { data } = await this.$axios.$get('/front/header/folders/list')
    commit('SET_MAINMENU', data)
  }
}
